# Zorgpaden jeugdzorg
Deze repository bevat de code voor het project Zicht op Zorgpaden. Dit project heeft als doel om, d.m.v. het berichtenverkeer tussen een zorgaanbieder en de gemeente, 
inzicht te geven op het gedrag van cliënten in de jeugdzorg. Dit gedrag wordt ook wel eens de 'customer journey' genoemd: welke stappen doorloopt een cliënt vanaf het moment 
dat er een zorgaanvraag wordt gedaan tot het moment dat de cliënt uit zorg gaat?

### Inhoud
De code zorgt voor het inlezen van Excel-documenten van het gestandaardiseerde berichtenverkeer (berichten 301, 303 en 307), 
het koppelen van deze documenten aan een document genaamd `Productcodes.XLS` wat per productcode meer informatie geeft over 
wat voor product het betreft. Daarna worden deze gemaakte tabellen middels enkele tussenstappen omgevormd tot grafieken en tabellen op een dashboard dat u op uw pc kunt aanzetten.

### Voorwaarden
- Een plek om deze map en de bijbehorende scripts op te slaan.
- Het berichtenverkeer in een Excel-document van de berichtcodes 301, 303 en 307.
- R en RStudio zijn geïnstalleerd op de computer. [Download R](https://cran.r-project.org/bin/windows/base/), [download RStudio](https://www.rstudio.com/products/rstudio/download/#download).

Voorbeelddocumenten voor hoe het berichtenverkeer eruit moet zien om het correct in te lezen staan in dezelfde map als de code: [301](https://gitlab.com/vng-realisatie/data-science-hub-zorgpaden/-/blob/master/zorgpaden/301_voorbeeld.xlsx), 
[303](https://gitlab.com/vng-realisatie/data-science-hub-zorgpaden/-/blob/master/zorgpaden/303_voorbeeld.xlsx), 
en [307](https://gitlab.com/vng-realisatie/data-science-hub-zorgpaden/-/blob/master/zorgpaden/307_voorbeeld.xlsx)

**Let op!**
- De kolomnamen van de voorbeelddocumenten moeten exact gebruikt worden! Meer kolommen mag, en volgorde is niet belangrijk.
- Van 307.xlsx wordt de 3e sheet gebruikt!
- Wij gebruiken de termen (ook als kolomnamen) 'Besluitnummer', 'Volgnummer indicatie', 'Datum indicatiebesluit', 
en 'VerwijzerID' waar anderen mogelijk respectievelijk 'Beschikkingnummer', 'Toewijzingnummer', 'Datum Toewijzing' en 'AGB Code Verwijzer' gebruiken!
- Ga ervan uit dat alle kolommen volledig gevuld moeten zijn! Dat is het veiligst en voorkomt de meeste problemen. (behalve de kolom VerwijzerID)

### Gebruik
Om het dashboard te kunnen draaien op uw computer moet u de volgende stappen doorlopen:
1. Zet de Excel-documenten van het berichtenverkeer in de map `zorpaden datasets`, met de namen `301.xlsx`, `303.xlsx` en `307.xlsx`;
2. Open vervolgens met RStudio het script `server.R` in de map `zorgpaden`;
3. Klik in RStudio op `Source` in de tab `Code` (of klik op `Run App` rechtsboven in de hoek van het scherm met de code);
4. Wacht tot het dashboard volledig geladen is (eerst krijg je het scherm van het dashboard te zien, maar wacht tot de data ook geladen is.);
5. Enjoy!
6. Om de applicatie te stoppen, sluit je het tabblad af en moet je in RStudio op het kleine rode stopbord klikken in de "console" onderaan het scherm.

### Contact
Neem contact met ons op in geval van vragen of opmerkingen.

Voor technische vragen, mail naar oscar.vancapel@vng.nl

Voor project-gerelateerde vragen, mail naar janneke.lummen@vng.nl

### Licentie
[![Creative Commons-Licentie](https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png)](https://creativecommons.org/licenses/by-nc-sa/4.0/deed.nl)

Dit werk valt onder een [Creative Commons Naamsvermelding-NietCommercieel-GelijkDelen 4.0 Internationaal-licentie](https://creativecommons.org/licenses/by-nc-sa/4.0/deed.nl)
